FROM python:3.6-alpine

LABEL maintainer="bdgwsh@gmail.com"

RUN apk upgrade && apk add --no-cache libpq libjpeg-turbo zlib
COPY requirements.txt /requirements.txt

RUN apk add --no-cache --virtual build-dependencies \
                                # core deps
                                linux-headers \
                                gcc \
                                python3-dev \
                                musl-dev \
                                postgresql-dev \
                                # pillow deps
                                jpeg-dev\
                                zlib-dev \
  && rm -rf /var/cache/apk/* \
  && pip install  --no-cache-dir  -r requirements.txt \
  && apk del build-dependencies


WORKDIR /app/
COPY . /app/

EXPOSE 8000
