"""
This file provides some shortcuts and utils.
"""
import sys
from io import BytesIO

from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile

from .constants import MIN_IMG_WIDTH, MIN_IMG_HEIGHT, MAX_IMG_WIDTH, MAX_IMG_HEIGHT


def width_is_larger(actual_width, max_width):
    """
    Checks if width is larger than max.
    :param actual_width: width of the image.
    :param max_width: Maximum image width.
    :return: bool
    """
    return actual_width > max_width


def width_is_less(actual_width, min_width):
    return actual_width < min_width


def height_is_larger(actual_height, max_height):
    return actual_height > max_height


def height_is_less(actual_height, min_height):
    return actual_height < min_height


def width_in_range(actual_width, min_width, max_width):
    return min_width <= actual_width <= max_width


def height_in_range(actual_height, min_height, max_height):
    return min_height <= actual_height <= max_height


def resize_img_to_appropriate(image):
    """
    Function for resizing the image for appropriate size.
    :param image:
    :return:
    """
    img = Image.open(image.file).convert('RGB')
    width, height = img.size
    resize_width, resize_height = MIN_IMG_WIDTH, MIN_IMG_HEIGHT
    if width_is_less(width, MIN_IMG_WIDTH) and height_in_range(height, MIN_IMG_HEIGHT, MAX_IMG_HEIGHT):
        resize_width = MIN_IMG_WIDTH
    elif width_is_larger(width, MAX_IMG_WIDTH) and height_in_range(height, MIN_IMG_HEIGHT, MAX_IMG_HEIGHT):
        resize_width = MAX_IMG_WIDTH

    if height_is_less(height, MIN_IMG_HEIGHT) and width_in_range(width, MIN_IMG_WIDTH, MAX_IMG_WIDTH):
        resize_height = MIN_IMG_WIDTH
    elif height_is_larger(height, MAX_IMG_HEIGHT) and width_in_range(width, MIN_IMG_HEIGHT, MAX_IMG_WIDTH):
        resize_height = MAX_IMG_WIDTH

    exif = None
    if 'exif' in img.info:
        exif = img.info['exif']

    img = img.resize((resize_width, resize_height), Image.ANTIALIAS)
    output = BytesIO()
    if exif:
        img.save(output, format='JPEG', exif=exif, quality=90)
    else:
        img.save(output, format='JPEG', quality=90)
    output.seek(0)
    image = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % image.name, 'image/jpeg',
                                 sys.getsizeof(output), None)
    return image
