from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _

from .constants import (
    MAX_IMG_HEIGHT,
    MAX_IMG_WIDTH,
    MIN_IMG_HEIGHT,
    MIN_IMG_WIDTH
)
from .utils import (
    width_is_larger,
    width_is_less,
    height_is_larger,
    height_is_less
)


@deconstructible
class ImageResolutionValidator:
    """
    Image resolution validator for validating image size.
    """

    def __init__(self, min_height=MIN_IMG_HEIGHT, min_width=MIN_IMG_WIDTH,
                 max_height=MAX_IMG_HEIGHT, max_width=MAX_IMG_WIDTH):
        self.min_size = (min_height, min_width)
        self.max_size = (max_height, max_width)
        self.min_height = min_height
        self.min_width = min_width
        self.max_height = max_height
        self.max_width = max_width

    def __call__(self, image):
        w, h = get_image_dimensions(image)

        if width_is_less(w, self.min_width) and height_is_less(h, self.min_height):
            raise ValidationError(
                _('Minimum width and height must be %d and %d px. Your size is (%d, %d)') % (self.min_width,
                                                                                             self.min_height,
                                                                                             w,
                                                                                             h))

        if width_is_larger(w, self.max_width) and height_is_larger(h, self.max_height):
            raise ValidationError(
                _('Maximum height and width must be %d and %d px.Your size is (%d, %d)') % (self.max_height,
                                                                                            self.max_width,
                                                                                            w, h))

    def __eq__(self, other):
        return (
                isinstance(other, self.__class__)
                and self.min_size == other.min_size
                and self.max_size == other.max_size
        )
