import textwrap

from django.contrib.auth.models import User
from django.db import models
from django.utils.text import wrap

from .utils import (
    resize_img_to_appropriate
)
from .validators import ImageResolutionValidator


class Postcard(models.Model):
    """
    Model that represents a postcard consisting of:
    - text_style
    - text
    - image
    - author
    """
    D3 = '3D'
    CLASSIC = 'CLS'
    STYLES = (
        (D3, '3D'),
        (CLASSIC, 'CLS'),
    )

    style = models.CharField(
        max_length=3,
        choices=STYLES,
        default=D3,
    )
    text = models.TextField(max_length=512)
    author = models.ForeignKey(
        User, related_name='postcards',
        on_delete=models.CASCADE, null=False
    )
    image = models.ImageField(
        upload_to='postcards_images',
        null=False,
        blank=False,
        validators=[ImageResolutionValidator(320, 240, 2048, 1968)]
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def shortened_text(self, width=None):
        text = self.text
        if width:
            text = wrap(text, width)
        return text

    def save(self, *args, **kwargs):

        if self.pk is None:
            self.image = resize_img_to_appropriate(self.image)

        super(Postcard, self).save(*args, **kwargs)

    def __str__(self):
        shortened_text = textwrap.shorten(self.text, width=20, placeholder="...")
        return shortened_text
