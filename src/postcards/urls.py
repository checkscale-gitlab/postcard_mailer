from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register(r'', views.PostcardViewSet, base_name="postcard-viewset")


urlpatterns = [
    path('me/', views.MyPostcardsList.as_view(), name='user-postcards-list')
]
urlpatterns += router.urls
