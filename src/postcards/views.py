from django_filters import rest_framework as filters
from rest_framework import status, viewsets, permissions, generics
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from .filters import PostcardFilter
from .models import Postcard
from .serializers import PostcardSerializer, EmailSerializer
from .tasks import send_postcard_to_email


class PostcardViewSet(viewsets.ModelViewSet):
    """
    A viewset that provides:
    - a list of postcards
    - creating a postcard
    - deleting a postcard
    - editing a postcard
    """
    queryset = Postcard.objects.all()
    serializer_class = PostcardSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    authentication_classes = (JWTAuthentication,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PostcardFilter

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @action(methods=['post'], detail=True)
    def send(self, request, pk):
        postcard = self.get_object()

        if request.method == 'POST':
            sender = request.user
            email_to = request.data.get('email')
            serialzier = EmailSerializer(data=request.data)
            if serialzier.is_valid(raise_exception=True):
                data = {
                    'info': 'Processing sending email'
                }
                send_postcard_to_email.delay(
                    postcard.image.url,
                    sender.username,
                    author_name=postcard.author.username,
                    message_text=postcard.text,
                    email=email_to,
                    style=postcard.style
                )
                return Response(data=data, status=status.HTTP_202_ACCEPTED)

        raise MethodNotAllowed


class MyPostcardsList(generics.ListAPIView):
    """
    ListView that allows to see user's postcards.
    """
    serializer_class = PostcardSerializer
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (JWTAuthentication,)

    def get_queryset(self):
        """
        Filtering by the user.
        """
        user = self.request.user
        return Postcard.objects.filter(author=user)
