from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from postcards.models import Postcard

RGB_VALIDATORS = [MinValueValidator(0), MaxValueValidator(255)]


class PostcardCommonColor(models.Model):
    """
    Model that represents an entry about most common color in the postcard.
    """
    postcard = models.ForeignKey(
        Postcard,
        on_delete=models.CASCADE, null=False
    )
    r = models.IntegerField(verbose_name='Red', validators=RGB_VALIDATORS, default=0)
    g = models.IntegerField(verbose_name='Green', validators=RGB_VALIDATORS, default=0)
    b = models.IntegerField(verbose_name='Blue', validators=RGB_VALIDATORS, default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def color(self):
        """
        Get value in RGB tuple.
        :return tuple: (R, G, B)
        """
        return {self.r}, {self.g}, {self.b}

    def __str__(self):
        return f"postcard_id: {self.postcard.pk}, ({self.r}, {self.g}, {self.b})"
