import logging
from collections import Counter

from PIL import Image
from django.db.models.signals import post_save
from django.dispatch import receiver
from postcards.models import Postcard

from .models import PostcardCommonColor

logger = logging.getLogger(__name__)


@receiver(post_save,  sender=Postcard)
def save_common_rgb_color(sender, instance, **kwargs):
    """
    Signal for saving most common color.
    """
    postcard_file = instance.image.file
    postcard_img = Image.open(postcard_file)
    pixels = list(postcard_img.getdata())
    counted_pixels = Counter(pixels)
    try:
        most_common_color = counted_pixels.most_common()[0]
    except IndexError as error:
        logger.error(f'Most common image pixel color error. Image is empty: {repr(error)}')
        return
    rgba = dict(zip('rgb', most_common_color[0]))
    common_color = PostcardCommonColor.objects.create(**rgba, postcard_id=instance.id)
    logger.info(f'Saved color value id: {common_color.pk}, value: {common_color.color}')
